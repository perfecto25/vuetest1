from flask import Flask, render_template, json, jsonify, request


app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html', greeting="hello from")

@app.route('/get_data')
def get_data():
    mydata = {"name": "Fred"}
    return json.dumps(mydata)
    #return render_template('index.html', dump=dump)


@app.route('/post_ajax', methods=['POST'])
def post_ajax():
    
    request_data = request.form.get('greeting')
    print('post data is')
    print(request_data)
    print(type(request_data))
    return request_data + ' and flask POST!!'
    
    #greeting = request_data.get('greeting', '')
    #print(greeting)
    #print(greeting+'and Flask')
    #return json.dumps(greeting + ' and from FLASK!!')    

@app.route('/get_ajax', methods=['GET'])
def get_ajax():
    data = {"name": "GET call"}
    return json.dumps(data)

if __name__ == '__main__':
    app.run(debug=True)